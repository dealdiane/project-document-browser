﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDocumentBrowser.Infrastructure
{
    public class ApplicationSettings
    {
        public string ProjectRootPath { get; set; }
    }
}
