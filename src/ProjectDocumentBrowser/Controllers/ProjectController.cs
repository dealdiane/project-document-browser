﻿using Microsoft.AspNet.Mvc;
using Microsoft.Framework.OptionsModel;
using ProjectDocumentBrowser.Infrastructure;
using ProjectDocumentBrowser.Models.Project;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectDocumentBrowser.Controllers
{
    public class FileSystemListViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(IEnumerable<Tuple<string, string>> directories)
        {
            return Invoke(directories, null);
        }

        public IViewComponentResult Invoke(IEnumerable<Tuple<string, string>> directories, IEnumerable<Tuple<string, string>> files)
        {
            return View(new ListViewModel
            {
                Directories = directories,
                Files = files,
            });
        }
    }

    public class ProjectController : Controller
    {
        private readonly IOptions<ApplicationSettings> _appSettings;

        public ProjectController(IOptions<ApplicationSettings> appSettings)
        {
            _appSettings = appSettings;
        }

        public IActionResult Browse(string path)
        {
            var absolutePath = GetAbsolutePath(path ?? String.Empty, true);
            var directories = Directory.GetDirectories(absolutePath)
                                                    .OrderBy(d => d)
                                                    .Select(p => Tuple.Create(Path.GetFileName(p), GetRelativePath(p)));

            var files = FindProjectFiles(absolutePath)
                .Select(p => Tuple.Create(Path.GetFileName(p), GetRelativePath(p)));

            return View(new IndexViewModel
            {
                Directories = directories,
                Files = files.ToList(),
            });
        }

        public IActionResult Open(string file)
        {
            var path = GetAbsolutePath(file, true);

            if (!FileExists(path))
            {
                return HttpNotFound();
            }

            if (!path.StartsWith(_appSettings.Value.ProjectRootPath, StringComparison.OrdinalIgnoreCase))
            {
                return HttpUnauthorized();
            }

            var files = FindProjectFiles(Path.GetDirectoryName(path));
            var index = files.FindIndex(f => String.Equals(path, f, StringComparison.OrdinalIgnoreCase));
            var nextFile = files[index + 1 >= files.Count ? 0 : index + 1];
            var prevFile = files[index - 1 < 0 ? files.Count - 1 : index - 1];

            var model = new OpenViewModel
            {
                PreviousFile = GetRelativePath(prevFile),
                CurrentFile = file,
                NextFile = GetRelativePath(nextFile),
            };

            var parent = Directory.GetParent(path);

            while (parent != null)
            {
                var bugHerdFile = Path.Combine(parent.FullName, "bugherd.key");

                if (FileExists(bugHerdFile))
                {
                    model.BugHerdKey = System.IO.File.ReadAllText(bugHerdFile);
                    break;
                }

                parent = parent.Parent;
            }

            return View(model);
        }

        public IActionResult Preview(string file)
        {
            var path = GetAbsolutePath(file);

            if (!FileExists(path))
            {
                return HttpNotFound();
            }

            var extension = Path.GetExtension(file);
            var mimeType = "image/" + (extension.Equals(".jpg", StringComparison.OrdinalIgnoreCase) ? "jpeg" : "png");

            return File(System.IO.File.OpenRead(path), mimeType);
        }

        private static bool FileExists(string file)
        {
            return System.IO.File.Exists(file);
        }

        private static List<string> FindProjectFiles(string path)
        {
            return
                Directory.GetFiles(path, "*.jpg")
                    .Concat(Directory.GetFiles(path, "*.png"))
                    .OrderBy(p => p.ToLowerInvariant())
                    .ToList();
        }

        private static string GetProjectName(string relativePath)
        {
            return relativePath.TrimStart('/', '\\', '~').Split('/', '\\')[0];
        }

        private string GetAbsolutePath(string relativePath, bool isSetTitle = false)
        {
            if (isSetTitle && !String.IsNullOrWhiteSpace(relativePath))
            {
                ViewData["Title"] = GetProjectName(relativePath);
            }

            return Path.GetFullPath(Path.Combine(_appSettings.Value.ProjectRootPath, WebUtility.UrlDecode(relativePath)));
        }

        private string GetRelativePath(string absolutePath)
        {
            var rootPath = _appSettings.Value.ProjectRootPath;

            if (!absolutePath.StartsWith(rootPath, StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("Path is not a sub path of the root path.");
            }

            return Path.GetFullPath(absolutePath).Substring(rootPath.Length).Replace('\\', '/');
        }

        //private static IEnumerable<string> ListFilesInDirectory(string directory, "search)
        //{
        //    Directory.GetFiles(directory,
        //}
    }
}