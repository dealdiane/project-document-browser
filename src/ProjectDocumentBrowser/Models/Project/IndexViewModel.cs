﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDocumentBrowser.Models.Project
{
    public class IndexViewModel
    {
        public IEnumerable<Tuple<string, string>> Directories { get; set; }

        public IList<Tuple<string, string>> Files { get; set; }
    }
}