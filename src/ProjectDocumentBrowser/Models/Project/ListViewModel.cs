﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDocumentBrowser.Models.Project
{
    public class ListViewModel
    {
        public IEnumerable<Tuple<string, string>> Directories { get; set; }

        public IEnumerable<Tuple<string, string>> Files { get; set; }
    }
}