﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDocumentBrowser.Models.Project
{
    public class OpenViewModel
    {
        public string BugHerdKey { get; set; }

        public string CurrentFile { get; set; }

        public string NextFile { get; set; }

        public string PreviousFile { get; set; }
    }
}